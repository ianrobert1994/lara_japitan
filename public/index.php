<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
--><?php
   session_start();
?> 
<!DOCTYPE html>
<html lang="en">
<head>
<title>Coast Pacific</title>
  <script language="javascript" type="text/javascript">
    
    {

      window.history.forward();

    }


  </script>
    <link rel="icon" href="img/cp_logo.png" type="image/gif" sizes="16x16">
	
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<meta name="keywords" content="Intrend Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->
	
	<link href="css/slider.css" type="text/css" rel="stylesheet" media="all"> 
	
	<!-- css files -->
	<link rel="stylesheet" href="css/bootstrap.css"> <!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" /> <!-- Style-CSS --> 
	<link rel="stylesheet" href="css/fontawesome-all.css"> <!-- Font-Awesome-Icons-CSS -->
	<!-- //css files -->
	
	<!-- testimonials css -->
	<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" property="" /><!-- flexslider css -->
	<!-- //testimonials css -->

	<!-- web-fonts -->
	<link href="//fonts.googleapis.com/css?family=Poiret+One&amp;subset=cyrillic,latin-ext" rel="stylesheet">


	<!-- //web-fonts -->


	
</head>

<style>
#wrapper{
	height: 600px;
}
.svg-wrapper{
	width: 100%;
	margin-bottom: 5px;
}
.svg-wrapper image{
	height: auto;
	width: 100%;
}

#foot{
	width: 100%; 
	height: auto;
}






@media only screen and (max-width: 800px) {
	.svg-wrapper{
	margin-bottom: 5px;
}
	#wrapper{
	height: 280px;
}

		


}

/*footer*/


* {
    box-sizing: border-box;
}

.header {
    text-align: center;

}

.row {
    display: -ms-flexbox; /* IE10 */
    display: flex;
    -ms-flex-wrap: wrap; /* IE10 */
    flex-wrap: wrap;
    padding: 0 4px;
}

/* Create four equal columns that sits next to each other */
.column {
    -ms-flex: 25%; /* IE10 */
    flex: 25%;
    max-width: 25%;
    padding: 0 4px;
}

.column img {
    margin-top: 5px;
    vertical-align: middle;
    padding: 0 20px;
}


/* Responsive layout - makes a two column-layout instead of four columns */
@media screen and (max-width: 800px) {
    .column {
        -ms-flex: 50%;
        flex: 50%;
        max-width: 50%;
    }


}

/* Responsive layout - makes the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
    .column {
        -ms-flex: 100%;
        flex: 100%;
        max-width: 100%;
    }
}


.welcome {
    display: inline;
    float: left;
    background: #fff;

}





</style>


<body>
<div class="welcome" style="text-transform: uppercase;"> <h3>WELCOME<strong>  <?php echo $_SESSION['fullname']; ?></h3></strong></div>
<div class="welcome" style="float: right; font-size: 20px; padding: 1px;"><a style="color: #000;"href="logout.php">Logout</a></div>


<!-- banner -->
	
		<div class="cd-radial-slider-wrapper"> 
		   
<!--Header-->
<header>
	<div class="container agile-banner_nav">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			
			<h1><a  href="index.html" style="margin-left: -80px"><img src="img/cp.png" style="height:40px; width:auto; "><span class="display"></span></a></h1>
			<button  class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent" style="margin-right: -200px;">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item ">
						<a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="about.php">About</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="services.php">News</a>
					</li>
					<li class="dropdown nav-item">
						<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">Outdoor
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu agile_short_dropdown">
							<li>
								<a href="o-living.php">Living</a>
							</li>
							<li>
								<a href="o-dining.php">Dining</a>
							</li>
							<li>
								<a href="o-chairs.php">Dining Chairs</a>
							</li>
							<li>
								<a href="o-accents.php">Accents</a>
							</li>
							<li>
								<a href="o-bar.php">Bar</a>
							</li>
							<li>
								<a href="o-garden.php">Garden</a>
							</li>
							<li>
								<a href="o-pool.php">Pool</a>
							</li>
							<li>
								<a href="o-servsta.php">Service Station</a>
							</li>
							<li>
								<a href="o-planters.php">Planters</a>
							</li>
							<li>
								<a href="o-lamps.php">Lamps</a>
							</li>
							<li>
								<a href="o-bags.php">Bags</a>
							</li>
						</ul>
					</li>

					<li class="dropdown nav-item">
						<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">Indoor 
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu agile_short_dropdown">
							<li>
								<a href="i-living.php">Living</a>
							</li>
							<li>
								<a href="i-bedroom.php">Bedroom</a>
							</li>
							<li>
								<a href="i-dining.php">Dining </a>
							</li> 
							<li>
								<a href="i-tables.php">Tables</a>
							</li>
							<li>
								<a href="i-accents.php">Accent Chairs</a>
							</li>
							<li>
								<a href="i-bench.php">Bench</a>
							</li>
							<li>
								<a href="i-stool.php">Stool & Ottoman</a>
							</li>
							<li>
								<a href="i-lamps.php">Lamps</a>
							</li>
							
						</ul>
					</li>

					
					<li class="nav-item">
						<a class="nav-link" href="contact.php">Contact</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="feedback.html" target="_blank">Feedback</a>
					</li>
				</ul>
			</div>
		  
		</nav>
	</div>
</header>
<!--Header-->

			
			<ul class="cd-radial-slider" data-radius1="60" data-radius2="1364" data-centerx1="110" data-centerx2="1290">
				<li class="visible"> 
					<div class="svg-wrapper">
						<svg viewBox="0 0 1400 800">
							<title>Animated SVG</title>
							<defs>
								<clipPath id="cd-image-1">
									<circle id="cd-circle-1" cx="110" cy="400" r="1364"/>
								</clipPath>
							</defs> 
							<image height='800px' width="1400px" clip-path="url(#cd-image-1)" xlink:href=" img/Outdoor/Living/h.png"></image>
						</svg>
					</div> <!-- .svg-wrapper --> 
					<div class="cd-radial-slider-content">
						<div class="wrapper">
							<div class="text-center">
								<h2>FURNITURE  DESIGN </h2>
								<h3> THE PERFECT PIECE </h3>
								<a href="about.html" class="read">Read More <i class="fas fa-caret-right"></i></a>
							</div>
						</div>
					</div> <!-- .cd-radial-slider-content -->
				</li> 
				<li class="next-slide">
					<div class="svg-wrapper">
						<svg viewBox="0 0 1400 800">
							<title>Animated SVG</title>
							<defs>
								<clipPath id="cd-image-2">
									<circle id="cd-circle-2" cx="1290" cy="400" r="60"/>
								</clipPath>
							</defs> 
							<image height='800px' width="1400px" clip-path="url(#cd-image-2)" xlink:href=" img/Indoor/bedroom/c.png"></image>
						</svg>
					</div>
					<div class="cd-radial-slider-content text-center">
						<div class="wrapper">
							<div class="text-center"> 
								<!-- <h3>FURNITURE  DESIGN  </h3> -->
								<h3>Furniture that inspires a lifestyle. </h3>
								<a href="about.html" class="read">Read More <i class="fas fa-caret-right"></i></a>
								
							</div>
						</div>
					</div>
				</li> 
				<li>
					<div class="svg-wrapper">
						<svg viewBox="0 0 1400 800">
							<title>Animated SVG</title>
							<defs>
								<clipPath id="cd-image-3">
									<circle id="cd-circle-3" cx="110" cy="400" r="60"/>
								</clipPath>
							</defs> 
							<image height='800px' width="1400px" clip-path="url(#cd-image-3)" xlink:href=" img/Indoor/Living/a.png"></image>
						</svg>
					</div> <!-- .svg-wrapper --> 
					<div class="cd-radial-slider-content text-center">
						<div class="wrapper">
							<div class="text-center"> 
								<h3>FURNITURE  DESIGN </h3>
								<h3> PERFECT </h3>
								<a href="about.html" class="read">Read More <i class="fas fa-caret-right"></i></a>
							</div>
						</div>
					</div> <!-- .cd-radial-slider-content -->
				</li> 
				<li class="prev-slide">
					<div class="svg-wrapper">
						<svg viewBox="0 0 1400 800">
							<title>Animated SVG</title>
							<defs>
								<clipPath id="cd-image-4">
									<circle id="cd-circle-4" cx="110" cy="400" r="60"/>
								</clipPath>
							</defs> 
							<image height='800px' width="1400px" clip-path="url(#cd-image-4)" xlink:href=" img/Outdoor/Bar/z.png "></image>
						</svg>
					</div> <!-- .svg-wrapper --> 
					<div class="cd-radial-slider-content text-center">
						<div class="wrapper">
							<div class="text-center"> 
								<h3>FURNITURE  DESIGN </h3>
								<h3> COMFORTABLE </h3>
								<a href="about.html" class="read">Read More <i class="fas fa-caret-right"></i></a>
							</div>
						</div>
					</div> <!-- .cd-radial-slider-content -->
				</li> 
			</ul> <!-- .cd-radial-slider --> 
			<ul class="cd-radial-slider-navigation">
				<li><a href="#0" class="next"><i class="fas fa-chevron-right"></i></a></li>
				<li><a href="#0" class="prev"><i class="fas fa-chevron-left"></i></a></li>
			</ul> <!-- .cd-radial-slider-navigation -->
		</div> <!-- .cd-radial-slider-wrapper --> 
	</div>
<!-- //banner --> 



<!-- about -->
<section class="wthree-row py-5">
	<div class="container py-lg-5 py-3">
		<h3 class="heading text-capitalize mb-sm-5 mb-4"> About us </h3>
		<div class="row d-flex justify-content-center">
			<div class="card col-lg-3 col-md-6 border-0">
				<div class="card-body bg-light pl-0 pr-0 pt-0">
					<h5 class=" card-title titleleft">Company Profile</h5>					
					<p class="card-text mb-3"> Coast Pacific Manufacturing Corporation was established in 1993, concentrating on classical rattan and wicker furnitures. Within five years of operations, the company received the 1998 Golden Shell Award for “Design Excellence” during the National Export Congress at Philippine Trade Training Center, organized by the Department of Trade & Industry. Through the years, the company has experienced continuous growth and development by using various materials to expand its product portfolio.  </p>
		
				</div>
				<img class="card-img-top" src="img/Indoor/Living/b.png" >
			</div>
			<div class="card col-lg-3 col-md-6 border-0">
				<br>
				
				

				<div class="card-body bg-light pl-0 pr-0 pt-0">
					<h5 class=" card-title titleleft"></h5>
					<p class="card-text mb-3"> Coast Pacific is privileged to have proficient and highly motivated team members who combine established techniques with superior materials, paying special attention to detail and hand-building each piece for home or commercial use. Serving wholesalers, retailers, contract firms, and direct buyers from both local and international markets, Coast Pacific is proud to provide customers with choices that reflect distinctive style and a passion for good design. </p>
				
					
				</div>
				<img class="card-img-top" src="img/Indoor/Living/c11.png" alt="Card image cap">
			</div>
			<div class="card col-lg-3 col-md-6 border-0">
				<br>
				<div class="card-body bg-light pl-0 pr-0 pt-0">
					<h5 class=" card-title titleleft"></h5>
					<p class="card-text mb-3"> Get to experience our products at these locations:
					Cannes Film Cruise Ship – Cannes, France; Hampton Inn New Smyrna Beach – Florida, USA; Hilton Hotel – Las Vegas, Nevada, USA; Hotel Jen OrchardGateway – Singapore; Le Meridien – Dubai, UAE; Marco Polo – Hong Kong; Okinawa Monterey Hotel – Okinawa, Japan; Marriott M-Resort – Las Vegas, USA; Velassaru Maldives – Republic of Maldives; Raffles Hotel Wafi – Dubai, UAE.  </p>
					
				</div>

				<img class="card-img-top" src="img/Indoor/Living/e.png" alt="Card image cap">

			</div>

		<div class="card col-lg-3 col-md-6 border-0">
				<br>
				
				<div class="card-body bg-light pl-0 pr-0 pt-0">
					<h5 class=" card-title titleleft"></h5>
					<p class="card-text mb-3">Anvaya Cove Beach & Nature Club - Morong, Bataan; Astoria Boracay – Boracay Island, Malay, Aklan; Busuanga Bay Lodge Inc. – Coron, Palawan; Discovery Shores Boracay – Boracay Island, Malay, Aklan; Fort Ilocandia Casino & Hotel – Laoag, Ilocos Norte; Marco Polo Hotel Davao – Davao City; Shangri-La Mactan – Mactan Island, Cebu; Solaire Resort & Casino Manila – Paranaque City; Thunder Bird Resorts – San Fernando, La Union & Rizal; Waterfront Hotel – Cebu City, Mactan Airport & Davao City.  </p>
				
					
				</div>
				<img class="card-img-top" src="img/Indoor/Living/d.png" alt="Card image cap">
		</div>

		</div>
	</div>


</section>




<!-- footer -->
<div class="py-5" style="background: white; border-top: 1px solid black;">
	<div class="container py-xl-2">
			<div class="row">

  <div class="column">
       <a href="http://www.coast-pacific.com/"> <img src="img/logo/3.png" id="foot" style="margin-top: 20px;"></a>
       </div>

   <div class="column">
   	<center>
       <a href="https://www.toyotamabolo.com/"> <img src="img/logo/7.jpg" id="foot" style="width: auto; height: 80px;"></a></center>
       </div>

      <div class="column">
       <a href="https://www.goldberrysuites.com/"> <img src="img/logo/4.png" id="foot"></a>
       </div>

	 <div class="column"><center>
       <a href="http://redbarkproperties.com/"><img src="img/logo/6.png" id="foot" style="width: auto; height: 80px;"></a></center>
       </div>
	
		</div>
	</div>
</div>






<!-- js-scripts -->		

	<!-- js -->
	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap --> 
	<!-- //js -->
	
	<!-- banner js -->
	<script src="js/snap.svg-min.js"></script>
	<script src="js/main.js"></script> <!-- Resource jQuery -->  
	<!-- //banner js --> 

	<!-- flexSlider --><!-- for testimonials -->
	<script defer src="js/jquery.flexslider.js"></script>
	<script type="text/javascript">
		$(window).load(function(){
		  $('.flexslider').flexslider({
			animation: "slide",
			start: function(slider){
			  $('body').removeClass('loading');
			}
		  });
		});
	</script>
	<!-- //flexSlider --><!-- for testimonials -->

	<!-- start-smoth-scrolling -->
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
	<!-- //here ends scrolling icon -->
	<!-- start-smoth-scrolling -->
	
<!-- //js-scripts -->

</body>
</html>