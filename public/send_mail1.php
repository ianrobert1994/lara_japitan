<?php
//Collect all inquiry data
$firstname = $_POST['firstname']; //required
$email =$_POST['email']; //required
$telephone =$_POST['telephone']; //NOT required
$message =$_POST['message']; //required

//Checking for valid email address
$error_message = '';
$email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';

if(!preg_match($email_exp,$email)) {
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
  }

if(!isset($firstname) ||
  !isset($email) ||
  !isset($telephone) ||
  !isset($message))
{
       echo "<script>alert('You did not fill out the required fields.');</script>";
}
//Email content
$email_to = 'mis@coast-pacific.com'; //contact form receiver
$email_header = 'From: '. $firstname ."\r\n";
$email_header .= "MIME-Version: 1.0\r\n"; //To make HTML tags work in the email body
$email_header .= "Content-Type: text/html; charset=ISO-8859-1\r\n"; //To make HTML tags work in the email body
$email_subject = 'Coast Pacific';
$email_body = "<html>
                            <body>
                            <h2>You have received an inquiry from <span style='color:#FF0000;font-weight:bold;'>" . $firstname ."</span></h2>
                            <h3 style='margin-bottom:5px;font-weight:bold;'>Details : </h3>
                            <table width='500' border='0' cellspacing='0' cellpadding='8'>
                            <tr>
                            <th align='left' width='35%'>Customer Name</th>
                            <td width='10%' align='center'>:</td>
                            <td>" . $email ."</td>
                            </tr>
                            <tr>
                            <th align='left'>Contact No.</th>
                            <td width='10%' align='center'>:</td>
                            <td>" . $telephone ."</td>
                            </tr>
                            <tr>
                            <th align='left'>Message</th>
                            <td width='10%' align='center'>:</td>
                            <td>" . $message ."</td>
                            </tr>
                            </table>
                            </body>
                            </html>";

//Ready to send the contact form information to email
mail($email_to,$email_subject,$email_body,$email_header) or die("Error!");

//After successful form submit
/*FOR REPAIR
echo "<script>";
echo "alert('We will be in touch to confirm your appointment, please keep your lines open. Thank you!');";
echo "window.location.href;";
echo "</script>";
*/ 
header("location:index-p.php"); 

?>