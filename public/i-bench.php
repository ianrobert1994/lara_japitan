<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
--><?php
   session_start();
?> 
<!DOCTYPE html>
<html lang="en">
<head>
<title>Bench - Indoor</title>
  <script language="javascript" type="text/javascript">
    
    {

      window.history.forward();

    }


  </script>
    <link rel="icon" href="img/cp_logo.png" type="image/gif" sizes="16x16">
	
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<meta name="keywords" content="Intrend Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->
	
	<!-- gallery css -->
	<link rel="stylesheet" href="css/swipebox.css">
	<!-- //gallery css -->

	<!-- css files -->
	<link rel="stylesheet" href="css/bootstrap.css"> <!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" /> <!-- Style-CSS --> 
	<link rel="stylesheet" href="css/fontawesome-all.css"> <!-- Font-Awesome-Icons-CSS -->
	<!-- //css files -->
	
	<!-- web-fonts -->
	<link href="//fonts.googleapis.com/css?family=Poiret+One&amp;subset=cyrillic,latin-ext" rel="stylesheet">
	<!-- //web-fonts -->
	
</head>
<style>
#wrapper{
	height: 600px;
}
.svg-wrapper{
	width: 100%;
	margin-bottom: 5px;
	table-layout: fixed;
}
.svg-wrapper image{
	height: auto;
	width: 100%;
	table-layout: fixed;
}


/* Overlay1 */
.overlay1 {
  position: relative;
display: block;


  
}

.overlay {
  position: absolute;
  bottom:1.5%;
  left: 0;
  right: 0;
  background-color: rgba(64, 64, 64, .3);
  overflow: hidden;
  width: 100%;
  height: 0;
  transition: .5s ease;

display: block;

  
  



}
.overlay1:hover .overlay {
  height: 13%;




}

.text {
  white-space: nowrap; 
  color: white;

  font-size: 20px;
  position: absolute;
  overflow: hidden;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);

}
/* end overlay1 */


@media only screen and (max-width: 800px) {
	.svg-wrapper{
	margin-bottom: 5px;
}
	#wrapper{
	height: 280px;
}
}



* {
    box-sizing: border-box;
}

.header {
    text-align: center;

}

.row {
    display: -ms-flexbox; /* IE10 */
    display: flex;
    -ms-flex-wrap: wrap; /* IE10 */
    flex-wrap: wrap;
    padding: 0 4px;
}

/* Create four equal columns that sits next to each other */
.column {
    -ms-flex: 25%; /* IE10 */
    flex: 25%;
    max-width: 25%;
    padding: 0 4px;
}

.column img {
    margin-top: 5px;
    vertical-align: middle;
    padding: 0 20px;
}

/* Responsive layout - makes a two column-layout instead of four columns */
@media screen and (max-width: 800px) {
    .column {
        -ms-flex: 50%;
        flex: 50%;
        max-width: 50%;
    }
}

/* Responsive layout - makes the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
    .column {
        -ms-flex: 100%;
        flex: 100%;
        max-width: 100%;
    }
}
.welcome {
	position:relative; 
	bottom: 33px;
	color: #000; 
	float:right; 
	font-size: 20px;
	 margin:2px;
}
 
 .banner{
	 background: url(img/Indoor/Living/g11.png) no-repeat center;
    height: 20vw;
    background-size: cover;

}


</style>
<body>
<div style="text-transform: uppercase; overflow: auto;"> <h3>WELCOME<strong>  <?php echo $_SESSION['fullname']; ?></h3></strong></div>
<a href="logout.php" class="welcome">Logout</a>
	<div class="banner" id="home"> 	   
	<!--Header-->
		<header>
	<div class="container agile-banner_nav">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			
			<h1><a  href="index.html" style="margin-left: -80px"><img src="img/cp.png" style="height:40px; width:auto; "><span class="display"></span></a></h1>
			<button  class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent" style="margin-right: -200px;">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item ">
						<a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="about.php">About</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="services.php">News</a>
					</li>
					<li class="dropdown nav-item">
						<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">Outdoor
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu agile_short_dropdown">
							<li>
								<a href="o-living.php">Living</a>
							</li>
							<li>
								<a href="o-dining.php">Dining</a>
							</li>
							<li>
								<a href="o-chairs.php">Dining Chairs</a>
							</li>
							<li>
								<a href="o-accents.php">Accents</a>
							</li>
							<li>
								<a href="o-bar.php">Bar</a>
							</li>
							<li>
								<a href="o-garden.php">Garden</a>
							</li>
							<li>
								<a href="o-pool.php">Pool</a>
							</li>
							<li>
								<a href="o-servsta.php">Service Station</a>
							</li>
							<li>
								<a href="o-planters.php">Planters</a>
							</li>
							<li>
								<a href="o-lamps.php">Lamps</a>
							</li>
							<li>
								<a href="o-bags.php">Bags</a>
							</li>
						</ul>
					</li>

					<li class="dropdown nav-item">
						<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">Indoor 
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu agile_short_dropdown">
							<li>
								<a href="i-living.php">Living</a>
							</li>
							<li>
								<a href="i-bedroom.php">Bedroom</a>
							</li>
							<li>
								<a href="i-dining.php">Dining </a>
							</li> 
							<li>
								<a href="i-tables.php">Tables</a>
							</li>
							<li>
								<a href="i-accents.php">Accent Chairs</a>
							</li>
							<li>
								<a href="i-bench.php">Bench</a>
							</li>
							<li>
								<a href="i-stool.php">Stool & Ottoman</a>
							</li>
							<li>
								<a href="i-lamps.php">Lamps</a>
							</li>
							
						</ul>
					</li>

					
					<li class="nav-item">
						<a class="nav-link" href="contact.php">Contact</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="feedback.html" target="_blank">Feedback</a>
					</li>
				</ul>
			</div>
		  
		</nav>
	</div>
</header>
	<!--Header-->
</div>



<div class="container py-sm-3">
	<h2 class="heading text-capitalize mb-sm-5 mb-3"> BENCH FURNITURE - INDOOR</h2>
	<div class="gallery">

					<div class="overlay1">
					<div class="svg-wrapper" id="wrapper">
						<svg viewBox="0 0 1400 800"> 
							<image height='800px' width="1400px" clip-path="url(#cd-image-1)" xlink:href=" img/Indoor/Bench/a.png"></image>
							<div class="overlay">
    					<div class="text"><strong>Hello World:</strong></div>
    						</div>
						</svg>

					</div> <!-- .svg-wrapper -->
				</div>

					<div class="overlay1">
					<div class="svg-wrapper"  id="wrapper">
						<svg viewBox="0 0 1400 800"> 
							<image height='800px' width="1400px" clip-path="url(#cd-image-1)" xlink:href=" img/Indoor/Bench/b.png"></image>
							<div class="overlay">
    					<div class="text"><strong>Hello World:</strong></div>
    						</div>
						</svg>
					</div> <!-- .svg-wrapper -->
				</div>

					
					<div class="overlay1">
					<div class="svg-wrapper"  id="wrapper">
						<svg viewBox="0 0 1400 800"> 
							<image height='800px' width="1400px" clip-path="url(#cd-image-1)" xlink:href=" img/Indoor/Bench/c.png"></image>
							<div class="overlay">
    					<div class="text"><strong>Hello World:</strong></div>
    						</div>
						</svg>
					</div> <!-- .svg-wrapper -->
				</div>

					<div class="overlay1">
					<div class="svg-wrapper"  id="wrapper">
						<svg viewBox="0 0 1400 800" > 
							<image height='800px' width="1400px" clip-path="url(#cd-image-1)" xlink:href=" img/Indoor/Bench/d.png"></image>
							<div class="overlay">
    					<div class="text"><strong>Hello World:</strong></div>
    						</div>
						</svg>
					</div> <!-- .svg-wrapper -->
				</div>	
		
					

					
	</div>
</div>

<!-- .gallery -->

<!-- footer -->
<div class="py-5" style="background: white; border-top: 1px solid black;">
	<div class="container py-xl-2">
			<div class="row">

  <div class="column">
       <a href="http://www.coast-pacific.com/"> <img src="img/logo/3.png" id="foot" style="margin-top: 20px;"></a>
       </div>

   <div class="column">
   	<center>
       <a href="https://www.toyotamabolo.com/"> <img src="img/logo/7.jpg" id="foot" style="width: auto; height: 80px;"></a></center>
       </div>

      <div class="column">
       <a href="https://www.goldberrysuites.com/"> <img src="img/logo/4.png" id="foot"></a>
       </div>

	 <div class="column"><center>
       <a href="http://redbarkproperties.com/"><img src="img/logo/6.png" id="foot" style="width: auto; height: 80px;"></a></center>
       </div>
	
		</div>
	</div>
</div>


<!-- footer -->

<!-- js-scripts -->		

	<!-- js -->
	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap --> 
	<!-- //js -->
	
	<!-- swipe box js -->
	<script src="js/jquery.swipebox.min.js"></script> 
	    <script type="text/javascript">
			jQuery(function($) {
				$(".swipebox").swipebox();
			});
	</script>
	<!-- //swipe box js -->

	<!-- start-smoth-scrolling -->
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
	<!-- //here ends scrolling icon -->
	<!-- start-smoth-scrolling -->
	
<!-- //js-scripts -->

</body>
</html>