<?php  include('server.php');

	if (isset($_GET['edit'])){
		$id = $_GET['edit'];
		$edit_state= true;
		$rec= mysqli_query($db,"SELECT * FROM users WHERE id=$id");
		$record = mysqli_fetch_array($rec);
		$fullname = $record['fullname'];
		$email = $record['email'];
		$company = $record['company'];
		$contact = $record['contact'];
		$remarks = $record['remarks'];
		$password = $record['password'];
		$id = $record['id'];


	}

 ?>



<!DOCTYPE html>
<html>
<head>
	<title> list of users  admin</title>

	<link rel="stylesheet" type="text/css" href="style.css">

</head>

<style>

.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    margin: 4px 2px;
    cursor: pointer;
}

.button1 {
	font-size: 10px;
	margin-left: 70%;
}




</style>

<body>

	<?php if(isset($_SESSION['msg'])): ?>
	<div class="msg">
		<?php
			echo ($_SESSION['msg']);
			unset($_SESSION['msg']);
		?>
	</div>
<?php endif ?>

			<center><div style="color:blue;margin-bottom: -10px;"><a> ADMIN USER VIEW ACCOUNT </a></div></center>

				<a href="adminpending.php"><button class="button button1">User Pending Request </button></a>
				<a href="admin-login.php"><button class="button button1" style="background-color:blue;margin-top: -4.4px;">LOGOUT </button></a>

			<form method="post" action="server.php" style="margin-top:5px;">
				<input type="hidden" name="id" value="<?php echo $id;?>">
				<div class="input-group">
					<label>Fullname</label>
					<input type="text" name="fullname" value="<?php echo $fullname?>">
				</div>
				<div class="input-group">
					<label>Email</label>
					<input type="text" name="email"value="<?php echo $email?>">
				</div>
				<div class="input-group">
					<label>Company</label>
					<input type="text" name="company"value="<?php echo $company?>">
				</div>
				<div class="input-group">
					<label>Contact</label>
					<input type="text" name="contact"value="<?php echo $contact?>">
				</div>
				<div class="input-group">
					<label>remarks</label>
					<input type="text" name="remarks"value="<?php echo $remarks?>">
				</div>
				<div class="input-group">
					<label>Password</label>
					<input type="text" name="password"value="<?php echo $password?>">
				</div>
				<div class="input-group">
				<?php if($edit_state == false):?>
				<button type="submit" name="save" class="btn">Save</button>
				<?php else:?>
				<button type="submit" name="update" class="btn">Update</button>
				<?php endif?>	

					
				</div>
			</form>


				<table>
		<thead>
			<tr>
				<th>Fullname </th>
				<th>Email</th>
				<th>Company </th>
				<th>Contact </th>
				<th>Remarks </th>
				<th>Password </th>
				<th colspan="2">Action</th>

			</tr>
		</thead>
		<tbody>
			<?php while ($row = mysqli_fetch_array($results)) { ?>
		<tr>
			<td><?php echo $row['fullname']; ?></td>
			<td><?php echo $row['email']; ?></td>
			<td><?php echo $row['company']; ?></td>
			<td><?php echo $row['contact']; ?></td>
			<td><?php echo $row['remarks']; ?></td>
			<td><?php echo $row['password']; ?></td>
			
			<td>
				<a href="admin.php?edit=<?php echo $row['id']; ?>" class="edit_btn" >Edit</a>
			</td>
			<td>
				<a href="server.php?del=<?php echo $row['id']; ?>" class="del_btn">Delete</a>
			</td>
		</tr>
	<?php } ?>


		</tbody>
	</table>
	<script language="javascript" type="text/javascript">
		
		{

			window.history.forward();

		}


	</script>
	
</body>
</html>

