<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePendingreqTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pendingreq', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('fullname', 100);
			$table->string('email', 100);
			$table->string('company', 100);
			$table->integer('contact');
			$table->string('remarks', 250);
			$table->string('password', 100);
			$table->integer('user_id')->nullable()->index('user_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pendingreq');
	}

}
